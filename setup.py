from setuptools import setup

setup(
    name='coeapp',
    version='0.1',
    description='a pip-installable package for coe deal to be seatle',
    license='MIT',
    packages=['coeapp'],
    author='Sylvio Campos Neto',
    author_email='sdcampos@santander.com.br',
    keywords=['coe', 'coeapp', 'Deals to be Settle'],
    url='https://gitlab.com/s.campos/coeapp',
    install_requires=['pandas', 'tqdm', 'beautifulsoup4', 'numpy', 'pypiwin32'],
    zip_safe=False
)