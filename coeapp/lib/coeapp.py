# -*- coding: utf-8 -*-
"""
Created on Mon Fev 21 15:51:00 2018

@author: t684098 - Sylvio Campos Neto
"""

from bs4 import BeautifulSoup
import requests
import datetime
import sqlite3
import numpy as np
from numbers import Number
import sys

class Cetip(object):
    """
    Function library for CETIP acquittance and calculation
    """

    @staticmethod
    def tdi(di):
        """
        CETIP TDI calculation method
        :param di: entrar com o di em valor absoluto
        :return: devolve valor TDI calculado
        """
        return round((1 + di / 100) ** (1 / 252), 15)

    @classmethod
    def get_di(cls):
        """
        Get daily CDI tax from www.cetip.com.br
        :return: Tupla data e di do dia
        """
        page = requests.get("http://www.cetip.com.br")
        soup = BeautifulSoup(page.content, 'html.parser')
        di = soup.find(id="ctl00_Banner_lblTaxDI").text
        dt = soup.find(id="ctl00_Banner_lblTaxDateDI").text
        dt = datetime.datetime.strptime(dt[1:11], '%d/%M/%Y').strftime('%Y-%M-%d')
        di = float(di[:1] + '.' + di[2:4])
        return [dt, di]

    @staticmethod
    def tax_di(df_cdi, dt_ini, dt_final, vol_ini):
        indexes = df_cdi[(df_cdi['Data'] >= dt_ini) & (df_cdi['Data'] < dt_final)].index
        aux_df = df_cdi.loc[indexes, :]
        aux_df['Fator'] = np.vectorize(Cetip.tdi)(aux_df['DI'])
        aux_df['FatorAcumulado'] = aux_df.Fator.cumprod()
        fat_di = aux_df.loc[max(aux_df.index), 'FatorAcumulado']
        rend_di = fat_di * vol_ini
        return (rend_di - vol_ini) / vol_ini

    @staticmethod
    def tax_premium(vol_ini, vol_end):
        return (vol_end - vol_ini) / vol_ini


class Database(object):
    """
    Database object
    Default path class initiator
    Lib of db functions
    """
    default_path = r'\\WEBPGIHVWBR02\db\coe\cetip.db'

    def __init__(self, db_path=None):
        """
        Class initiator with database path.
        :param db_path: path to database
        """

        self.db_path = db_path

    def con_create(self):
        """
        Create Socket connection with SQL / SQLite3 database.
        :param path: Caminho do arquivo do banco de dados, caminho padrão: \\WEBPGIHVWBR02\db\coe\cetip.db
        :return: socket de conexão
        """
        if self.db_path is None:
            return sqlite3.connect(Database.default_path)
        else:
            return sqlite3.connect(self.db_path)

    @staticmethod
    def list_tables(socket):
        """
        List tables from database
        :param socket: Socket de conexão com o SQL
        :return: Socket error
        """
        try:
            cursor = socket.cursor()
            cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
            tbls = cursor.fetchall()
            return tbls
        except:
            return print("Error: Invalid Socket")

    @staticmethod
    def insert_di(socket, di):
        """
        Insert a new tuple of date and DI tax into Database.
        :param socket: Socket de conexão com SQL
        :param DI: Tupla com data e valor do DI
        :return: Sucesso ou falha na incerção
        """
        # Analise tupla DI [data, di]
        try:
            datetime.datetime.strptime(di[0], '%Y-%M-%d')
            pass
        except:
            return print("Formato de data inválido")

        if type(di[1]) == float:
            pass
        else:
            return print("Formato numérico inválido")

        # Teste de existencia da data
        cursor = socket.cursor()
        cursor.execute("select * from di where Data = ?", (di[0],))
        test_dt = cursor.fetchall()

        if len(test_dt) == 0:
            try:
                cursor.execute("insert into di values(?,?)", di)
                socket.commit()
                return print("Tupla adicionada com sucesso")
            except:
                return print("Erro na inclusão")

        else:
            return print("Data ja existente no banco de dados: {}".format(test_dt[0]))

class Utilities(object):
    """
    Utilities functions, useful for vectorized
    """
    @staticmethod
    def dt_correction(dt_noformat):
        """
        Correct the dates format from dd/mm/yyyy to yyyy-mm-dd
        :param dt_noformat: non formatted date
        :return: formatted date
        """
        return datetime.datetime.strptime(dt_noformat,'%d/%M/%Y').strftime('%Y-%M-%d')

    @staticmethod
    def str_to_float(eu_string):
        """
        Correct EU number string format to US number format
        :param eu_string: String in EU format
        :return: float in US format
        """
        return float(eu_string.replace(".", "").replace(",", "."))

    @staticmethod
    def IRS_cal_col(strike, strg):
        type_list_MenorQue = {"COECALLSPREAD": "DÓLAR PARTICIPAÇÃO ALTA", "COEPUTSPREAD": "DÓLAR PARTICIPAÇÃO BAIXA",
                              "COEDIGITALCALL": "DÓLAR PRÊMIO ALTA", "COEDIGITALPUT": "DÓLAR PRÊMIO BAIXA"}
        type_list_MaiorQue = {"COECALLSPREAD": "JUROS PARTICIPAÇÃO ALTA", "COEPUTSPREAD": "JUROS PARTICIPAÇÃO BAIXA",
                              "COEDIGITALCALL": "JUROS PRÊMIO ALTA", "COEDIGITALPUT": "JUROS PRÊMIO BAIXA"}
        if strike < 50:
            return type_list_MenorQue.get(strg)
        else:
            return type_list_MaiorQue.get(strg)

    @staticmethod
    def EQS_cal_col(strike, strg):
        type_list_MenorQue = {"COECALLSPREAD": "IBOVESPA PARTICIPAÇÃO ALTA",
                              "COEPUTSPREAD": "IBOVESPA PARTICIPAÇÃO BAIXA",
                              "COEDIGITALCALL": "IBOVESPA PRÊMIO ALTA", "COEDIGITALPUT": "IBOVESPA PRÊMIO BAIXA"}
        type_list_MaiorQue = {"COECALLSPREAD": "S&P PARTICIPAÇÃO ALTA", "COEPUTSPREAD": "S&P PARTICIPAÇÃO BAIXA"}
        if strike > 10000:
            return type_list_MenorQue.get(strg)
        else:
            return type_list_MaiorQue.get(strg)

    @staticmethod
    def cal_col_res(swap_opt, vol_ini, vol_end):
        if vol_ini == vol_end:
            return "Valor Nominal Aplicado"
        elif swap_opt == 'Digital':
            return "Valor Nominal Aplicado + Taxa Pré"
        elif swap_opt == 'Plain Vanilla':
            return "Valor Nominal Aplicado + Participação"

    @staticmethod
    def get_agencia(ccstrng):
        return str(ccstrng)[7:11]

    @staticmethod
    def as_percent(v, precision='0.2'):
        """Convert number to percentage string."""
        if isinstance(v, Number):
            return str("{{:{}%}}".format(precision).format(v)).replace('.', ',')
        else:
            raise TypeError("Numeric type required")

    @staticmethod
    def printprogressbar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█'):
        """
        Call in a loop to create terminal progress bar
        @params:
            iteration   - Required  : current iteration (Int)
            total       - Required  : total iterations (Int)
            prefix      - Optional  : prefix string (Str)
            suffix      - Optional  : suffix string (Str)
            decimals    - Optional  : positive number of decimals in percent complete (Int)
            length      - Optional  : character length of bar (Int)
            fill        - Optional  : bar fill character (Str)
        """
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        filledLength = int(length * iteration // total)
        bar = fill * filledLength + '-' * (length - filledLength)
        print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r')
        # Print New Line on Complete
        if iteration == total:
            print()

    @staticmethod
    def progressbar(count, total, status=''):
        bar_len = 60
        filled_len = int(round(bar_len * count / float(total)))
        percents = round(100.0 * count / float(total), 1)
        bar = '=' * filled_len + '-' * (bar_len - filled_len)
        sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
        sys.stdout.flush()
