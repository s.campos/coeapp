# -*- coding: utf-8 -*-

import win32com.client
import win32com
import datetime, time
import os

# Last xls file cleaning
file = os.listdir('\\\\webpgihvwbr02\\db\\coe\\mail_xls')
if len(file) > 0:
    os.remove("\\\\webpgihvwbr02\\db\\coe\\mail_xls\\{}".format(file[0]))
else:
    pass

if __name__ == "__main__":
    print("Mail - Deal to be Settle - Crawler")
    # Mail crawler
    outlook = win32com.client.Dispatch("Outlook.Application").GetNamespace("MAPI")
    inbox = outlook.GetDefaultFolder(6)
    messages = inbox.Items

    now = datetime.datetime.now()

    if now.month / 10 < 1:
        data = str(now.day) + "." + "0" + str(now.month) + "." + str(now.year)[2:4]
    else:
        data = str(now.day) + "." + str(now.month) + "." + str(now.year)[2:4]

    filter = "[Subject] = 'Relatório Deals to be Settle - COE {}'".format(data)
    message = messages.Find(filter)

    while message is None:
        time.sleep(1800)
        message = messages.Find(filter)

    print("xls file: %s" % (message.Attachments.Item(1).FileName))
    message.Attachments.Item(1).SaveAsFile('\\\\webpgihvwbr02\\db\\coe\\mail_xls\\' +
                                           message.Attachments.Item(1).FileName)
    print("File save success")
    print('\n')
    print("Press enter to exit...")
    val = input()