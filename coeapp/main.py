﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
# title           : main.py
# description     : This program displays an interactive menu on CLI for COE deals
# author          : Sylvio Campos Neto (@684098)
# date            : 2018-03-06
# version         : 0.1
# usage           : python main.py
# notes           :
# python_version  : 3.*
# =======================================================================
__author__ = "Sylvio Campos Neto - T684098"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Sylvio Campos Neto"
__email__ = "sdcampos@santander.com.br"
__status__ = "Production"

# Import the modules needed to run the script.
import sys, os
from coeapp import coeapp
from time import sleep
import pandas as pd
import numpy as np
import win32com.client as win32
from tqdm import tqdm
import json

# Mail load
with open(r'C:\Users\t684098\source\repos\coeapp\coeapp\lib\mail.txt', 'r') as file:
    mail_list = json.load(file)


# Main definition - constants
menu_actions = {}

# Main menu
def main_menu():
    os.system('cls')
    banner = """
******************** Santander (Brasil) S.A. ********************

                    Painel de vencimentos COE

Created on Fry Mar 02 11:25:00 2018
@author: t684098
*****************************************************************

<1> Update CETIP Tax database
<2> Create and display e-mail
<3> CETIP Database management (under dev)
<4> Exit
"""
    print(banner)
    choice = input("coe> ")
    exec_menu(choice)
    return


# Execute menu
def exec_menu(choice):
    os.system('cls')
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            menu_actions[ch]()
        except KeyError:
            print("Invalid selection, please try again.\n")
            menu_actions['main_menu']()
    return


# Menu 1 - Atualizar CETIP
def menu1():
    print("Making database socket")
    try:
        with tqdm(total=100) as pbar:
            db = coeapp.Database()
            pbar.update(50)
            con = db.con_create()
            pbar.update(50)
            pbar.close()
        print("Socket creation: Successful")
    except OSError:
        print("Socket creation: Fail")
    print("\n")
    print("CDI Crawler - CETIP")
    try:
        with tqdm(total=100) as pbar:
            di = coeapp.Cetip.get_di()
            pbar.update(50)
            print('Daily CDI tax: {}'.format(di))
            db.insert_di(con, di)
            pbar.update(50)
            pbar.close()
    except OSError:
        print('Web Crawler error')
    print("\n")
    print("<9> Back")
    print("<0> Exit")
    choice = input("coe> ")
    exec_menu(choice)
    return


# Menu 2 - Enviar relatório
def menu2():
    deals = os.listdir(r'\\WEBPGIHVWBR02\db\coe\mail_xls')
    print('Deals to process: {}'.format(deals[0]))
    print("Table Import")
    with tqdm(total=100) as pbar:
        try:
            #IRS Import
            IRS = pd.read_excel(r'\\WEBPGIHVWBR02\db\coe\mail_xls\{}'.format(deals[0]),
                                sheet_name="IRS", dtype='object')
            pbar.update(20)

            if len(IRS) == 0:
                pbar.update(20)
            else:
                # Insert Calculated columns on IRS
                IRS['Produto'] = np.vectorize(coeapp.Utilities.IRS_cal_col)(IRS['Strike 1'], IRS['Strategy Murex'])
                IRS['Resultado'] = np.vectorize(coeapp.Utilities.cal_col_res)(IRS['Swap Option'], IRS['Valor Bruto BP'],
                                                                              IRS['Valor Liquido COE'])
                IRS['Prazo da Aplicação'] = (IRS['End Date'] - IRS['Start Date']).dt.days

                to_float = ['Valor Bruto BP', 'Ajuste do Swap', 'IR COE', 'Valor Liquido COE', 'Original Notional']

                for float_key in to_float:
                    values = pd.to_numeric(IRS[float_key])
                    IRS.loc[:, float_key] = values

                IRS['Valor Liquido COE'] = IRS['Valor Liquido COE'] * -1
                IRS['Valor Bruto COE'] = (IRS['Valor Bruto BP'] + IRS['Ajuste do Swap']) * -1
                IRS['% Bruto'] = (IRS['Valor Bruto COE'] / IRS['Original Notional']) - 1
                pbar.update(20)

            # EQS Import
            EQS = pd.read_excel(r'\\WEBPGIHVWBR02\db\coe\mail_xls\{}'.format(deals[0]),
                                sheet_name="EQS", dtype='object')
            pbar.update(20)

            if len(EQS) == 0:
                pbar.update(20)
            else:
                # Insert Calculated Columns on EQS
                EQS['Produto'] = np.vectorize(coeapp.Utilities.EQS_cal_col)(EQS['Strike 1'], EQS['Strategy Murex'])
                EQS['Resultado'] = np.vectorize(coeapp.Utilities.cal_col_res)(EQS['Swap Option'], EQS['Valor Bruto BP'],
                                                                              EQS['Valor Liquido COE'])
                EQS['Prazo da Aplicação'] = (EQS['End Date'] - EQS['Start Date']).dt.days

                to_float = ['Valor Bruto BP', 'Ajuste do Swap', 'IR COE', 'Valor Liquido COE', 'Original Notional']

                for float_key in to_float:
                    values = pd.to_numeric(EQS[float_key])
                    EQS.loc[:, float_key] = values

                EQS['Valor Liquido COE'] = EQS['Valor Liquido COE'] * -1
                EQS['Valor Bruto COE'] = (EQS['Valor Bruto BP'] + EQS['Ajuste do Swap']) * -1
                EQS['% Bruto'] = (EQS['Valor Bruto COE'] / EQS['Original Notional']) - 1
                pbar.update(20)

            # Conditions to create the frame for the exit
            if len(IRS) == 0 and len(EQS) > 0:
                df_to_panel = EQS
            elif len(EQS) == 0 and len(IRS) > 0:
                df_to_panel = IRS
            else:
                df_to_panel = EQS.append(IRS)

            df_to_panel = df_to_panel.reset_index()

            col_to_panel = ['Produto', 'Resultado', 'Start Date', 'End Date', 'Counterparty',
                            'CPF/CNPJ Counterparty',
                            'Client. Account', 'Prazo da Aplicação', 'Original Notional', 'Strike 1',
                            'Valor Bruto COE',
                            'IR COE', 'Valor Liquido COE', '% Bruto']

            renames = {'Start Date': 'Data de Aplicação', 'End Date': 'Data de Vencimento',
                       'Counterparty': 'Nome do Cliente', 'CPF/CNPJ Counterparty': 'CPF',
                       'Client. Account': 'Número da C/C', 'Original Notional': 'Valor Nominal Aplicado',
                       'Strike 1': 'Valor Referência'}

            painel = df_to_panel.loc[:, col_to_panel]
            painel.rename(columns=renames, inplace=True)

            # Clean cache and memory
            del EQS, IRS, col_to_panel, df_to_panel, float_key, renames, to_float, values
            pbar.update(20)
            pbar.close()
        except OSError:
            print("Xls import error")
            pbar.update(20)
            pbar.close()

    try:
        with tqdm(total=100) as pbar2:
            # Call DI database taxes
            db = coeapp.Database()
            con = db.con_create()
            cdi = pd.read_sql_query('select * from di', con)
            pbar2.update(33)

            # CDI col calculation
            i = 0
            perc_di = []
            for i in range(len(painel)):
                rend_di = coeapp.Cetip.tax_di(cdi,
                                              str(painel.loc[i, 'Data de Aplicação'])[:10],
                                              str(painel.loc[i, 'Data de Vencimento'])[:10],
                                              painel.loc[i, 'Valor Nominal Aplicado'])
                rend_coe = coeapp.Cetip.tax_premium(painel.loc[i, 'Valor Nominal Aplicado'],
                                                    painel.loc[i, 'Valor Bruto COE'])
                perc_di.append(rend_coe / rend_di)
            pbar2.update(33)
            painel['% CDI'] = perc_di
            pbar2.update(34)
            pbar2.close()

    except OSError:
        print(" - Panel Composition Error")
        pbar2.close()

    print("Excel Output Composition")
    try:
        # Progress Bar Iteration
        for i in tqdm(range(5)):
            if i == 0:
                # Create Excel final file
                excel_path = "\\\\WEBPGIHVWBR02\\db\\coe\\mail_send\\" + "Vencimentos COE - " + str(
                    painel.loc[0, 'Data de Vencimento'])[:10] + ".xlsx"

            elif i == 1:
                # Print dataframe to Excel file
                writer = pd.ExcelWriter(excel_path, engine='xlsxwriter', datetime_format='dd/mm/yyyy')
                painel.to_excel(writer, index=False, sheet_name='COE')

            elif i == 2:
                # Workbook format
                workbook = writer.book
                worksheet = writer.sheets['COE']
                format1 = workbook.add_format({'num_format': '#,##0.00'})
                format2 = workbook.add_format({'num_format': '0.00%'})
                # Add a format. Light red fill with dark red text.
                format_zero = workbook.add_format({'bg_color': '#FFC7CE',
                                                   'font_color': '#9C0006'})
                # Add a format. Green fill with dark green text.
                format_Premio = workbook.add_format({'bg_color': '#C6EFCE',
                                                     'font_color': '#006100'})
                worksheet.set_column('I:M', None, format1)
                worksheet.set_column('N:O', None, format2)

            elif i == 3:
                # Results conditional format
                resultado = painel['Resultado'].values
                row = 1
                col = 1
                for res in resultado:
                    if res == "Valor Nominal Aplicado":
                        worksheet.write(row, col, res, format_zero)
                    else:
                        worksheet.write(row, col, res, format_Premio)
                    row += 1

            elif i == 4:
                # Workbook save
                writer.save()
            sleep(0.5)
    except OSError:
        print(" - Excel composition error[{}]".format(i))
    print("Mailing Composition")
    try:
        # Progress Bar iterations
        for i in tqdm(range(5)):
            if i == 0:
                # Outlook instance call
                outlook = win32.Dispatch('outlook.application')

            elif i == 1:
                # Mail body create
                mail = outlook.CreateItem(0)
                mail.SentOnBehalfOfName = mail_list['FromBox']
                mail.To = mail_list['to']
                mail.CC = mail_list['cc']
                mail.Subject = 'COE - Vencimento de Operações -' + " dia " + str(painel.loc[0, 'Data de Vencimento'])[
                                                                             :10]
                mail.Body = 'Caros bom dia, \n \n Segue anexo todas as operações de COE que venceram hoje, dia {}, ' \
                            'com suas respectivas rentabilidades. \n \n Qualquer dúvida estamos à disposição.'.format(
                    str(painel.loc[0, 'Data de Vencimento'])[:10])

            elif i == 2:
                # Excel Attachment
                attachment = excel_path
                mail.Attachments.Add(attachment)
            sleep(0.5)
        sleep(2)
        # Display mail composition for user
        mail.Display()
    except OSError:
        print(" - Mail composition Error[{}]".format(i))
    sleep(1)
    print("Saving Log...")
    # Progress Bar iterations
    for i in tqdm(range(4)):
        if i == 0:
            # Check log existence
            cursor = con.cursor()
            cursor.execute(
                'select * from enviados where data = "{}"'.format(str(painel.loc[0, 'Data de Vencimento'])[:10]))
            log_check = cursor.fetchall()
        sleep(0.5)

    if len(log_check) == 0:
        cursor.execute("""INSERT INTO enviados (nome, data) VALUES (?,?)""",
                       ["Vencimentos COE - " + str(painel.loc[0, 'Data de Vencimento'])[:10] + ".xlsx",
                        str(painel.loc[0, 'Data de Vencimento'])[:10]])
        con.commit()
        con.close()
        print("  - Log done")
    else:
        print(" - File has being already send")
        print(log_check)
        con.close()
    print("\n")
    print("<9> Back")
    print("<0> Exit")
    choice = input("coe> ")
    exec_menu(choice)
    return


# Menu 3 - Database management
def menu3():
    print("Menu under development")
    print("\n")
    print("<9> Back")
    print("<0> Exit")
    choice = input("coe> ")
    exec_menu(choice)
    return


# Exit program
def menu4():
    sys.exit()


# Back to main menu
def back():
    menu_actions['main_menu']()


# Exit program
def exit():
    sys.exit()


# Menu definition
menu_actions = {
    'main_menu': main_menu,
    '1': menu1,
    '2': menu2,
    '3': menu3,
    '4': menu4,
    '9': back,
    '0': exit,
}

# Main Program
if __name__ == "__main__":
    # Launch main menu
    main_menu()
