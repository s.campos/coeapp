# -*- coding: utf-8 -*-

from coeapp import coeapp

if __name__=="__main__":
    print("CDI Crawler - CETIP")
    print("Tax Database update")
    print("\n")
    print("Making database socket")
    try:
        db = coeapp.Database()
        con = db.con_create()
        print("Socket creation: Successful")
    except OSError:
        print("Socket creation: Fail")
    print("\n")
    try:
        di = coeapp.Cetip.get_di()
        print('Daily CDI tax: {}'.format(di))
        db.insert_di(con, di)
    except OSError:
        print('Web Crawler error')
    print('\n')
    print("Press enter to exit...")
    rt = input()