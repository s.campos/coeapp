# -*- coding: utf-8 -*-

from coeapp import coeapp
import pandas as pd
import numpy as np
import win32com.client as win32
import os
import json

# Mail load
with open(r'C:\Users\t684098\source\repos\coeapp\coeapp\lib\mail.txt', 'r') as file:
    mail_list = json.load(file)
deals = os.listdir(r'\\WEBPGIHVWBR02\db\coe\mail_xls')

if __name__ == "__main__":
    print("Mail - Deals to be Settle - Sender")
    print("\n")
    print('Deals to process: {}'.format(deals[0]))
    print(" - Table Import")
    try:
        #IRS Import
        IRS = pd.read_excel(r'\\WEBPGIHVWBR02\db\coe\mail_xls\{}'.format(deals[0]), sheet_name="IRS", dtype='object')
        if len(IRS) == 0:
            pass
        else:
            # Insert Calculated columns on IRS
            IRS['Produto'] = np.vectorize(coeapp.Utilities.IRS_cal_col)(IRS['Strike 1'], IRS['Strategy Murex'])
            IRS['Resultado'] = np.vectorize(coeapp.Utilities.cal_col_res)(IRS['Swap Option'], IRS['Valor Bruto BP'],
                                                                          IRS['Valor Liquido COE'])
            IRS['Prazo da Aplicação'] = (IRS['End Date'] - IRS['Start Date']).dt.days
            to_float = ['Valor Bruto BP', 'Ajuste do Swap', 'IR COE', 'Valor Liquido COE', 'Original Notional']
            for float_key in to_float:
                values = pd.to_numeric(IRS[float_key])
                IRS.loc[:, float_key] = values

            IRS['Valor Liquido COE'] = IRS['Valor Liquido COE'] * -1
            IRS['Valor Bruto COE'] = (IRS['Valor Bruto BP'] + IRS['Ajuste do Swap']) * -1
            IRS['% Bruto'] = (IRS['Valor Bruto COE'] / IRS['Original Notional']) - 1

        # EQS Import
        EQS = pd.read_excel(r'\\WEBPGIHVWBR02\db\coe\mail_xls\{}'.format(deals[0]), sheet_name="EQS", dtype='object')
        if len(EQS) == 0:
            pass
        else:
            # Insert Calculated Columns on EQS
            EQS['Produto'] = np.vectorize(coeapp.Utilities.EQS_cal_col)(EQS['Strike 1'], EQS['Strategy Murex'])
            EQS['Resultado'] = np.vectorize(coeapp.Utilities.cal_col_res)(EQS['Swap Option'], EQS['Valor Bruto BP'],
                                                                                  EQS['Valor Liquido COE'])
            EQS['Prazo da Aplicação'] = (EQS['End Date'] - EQS['Start Date']).dt.days

            to_float = ['Valor Bruto BP', 'Ajuste do Swap', 'IR COE', 'Valor Liquido COE', 'Original Notional']

            for float_key in to_float:
                values = pd.to_numeric(EQS[float_key])
                EQS.loc[:, float_key] = values

            EQS['Valor Liquido COE'] = EQS['Valor Liquido COE'] * -1
            EQS['Valor Bruto COE'] = (EQS['Valor Bruto BP'] + EQS['Ajuste do Swap']) * -1
            EQS['% Bruto'] = (EQS['Valor Bruto COE'] / EQS['Original Notional']) - 1

        # Conditions to create the frame for the exit
        if len(IRS) == 0 and len(EQS) > 0:
            df_to_panel = EQS
        elif len(EQS) == 0 and len(IRS) > 0:
            df_to_panel = IRS
        else:
            df_to_panel = EQS.append(IRS)

        df_to_panel = df_to_panel.reset_index()

        col_to_panel = ['Produto', 'Resultado', 'Start Date', 'End Date', 'Counterparty',
                        'CPF/CNPJ Counterparty',
                        'Client. Account', 'Prazo da Aplicação', 'Original Notional', 'Strike 1',
                        'Valor Bruto COE',
                        'IR COE', 'Valor Liquido COE', '% Bruto']

        renames = {'Start Date': 'Data de Aplicação', 'End Date': 'Data de Vencimento',
                   'Counterparty': 'Nome do Cliente', 'CPF/CNPJ Counterparty': 'CPF',
                   'Client. Account': 'Número da C/C', 'Original Notional': 'Valor Nominal Aplicado',
                   'Strike 1': 'Valor Referência'}

        painel = df_to_panel.loc[:, col_to_panel]
        painel.rename(columns=renames, inplace=True)

        # Clean cache and memory
        del EQS, IRS, col_to_panel, df_to_panel, float_key, renames, to_float, values

    except OSError:
        print("Xls import error")

    try:
        # Call DI database taxes
        db = coeapp.Database()
        con = db.con_create()
        cdi = pd.read_sql_query('select * from di', con)

        # CDI col calculation
        i = 0
        perc_di = []
        for i in range(len(painel)):
            rend_di = coeapp.Cetip.tax_di(cdi,
                                          str(painel.loc[i, 'Data de Aplicação'])[:10],
                                          str(painel.loc[i, 'Data de Vencimento'])[:10],
                                          painel.loc[i, 'Valor Nominal Aplicado'])
            rend_coe = coeapp.Cetip.tax_premium(painel.loc[i, 'Valor Nominal Aplicado'],
                                                painel.loc[i, 'Valor Bruto COE'])
            perc_di.append(rend_coe / rend_di)
        painel['% CDI'] = perc_di

    except OSError:
        print(" - Panel Composition Error")

    print(" - Excel Output Composition")
    try:
        # Create Excel final file
        excel_path = "\\\\WEBPGIHVWBR02\\db\\coe\\mail_send\\" + "Vencimentos COE - " + str(
            painel.loc[0, 'Data de Vencimento'])[:10] + ".xlsx"

        # Print dataframe to Excel file
        writer = pd.ExcelWriter(excel_path, engine='xlsxwriter', datetime_format='dd/mm/yyyy')
        painel.to_excel(writer, index=False, sheet_name='COE')

       # Workbook format
        workbook = writer.book
        worksheet = writer.sheets['COE']
        format1 = workbook.add_format({'num_format': '#,##0.00'})
        format2 = workbook.add_format({'num_format': '0.00%'})
        # Add a format. Light red fill with dark red text.
        format_zero = workbook.add_format({'bg_color': '#FFC7CE', 'font_color': '#9C0006'})
        # Add a format. Green fill with dark green text.
        format_Premio = workbook.add_format({'bg_color': '#C6EFCE', 'font_color': '#006100'})
        worksheet.set_column('I:M', None, format1)
        worksheet.set_column('N:O', None, format2)

        # Results conditional format
        resultado = painel['Resultado'].values
        row = 1
        col = 1
        for res in resultado:
            if res == "Valor Nominal Aplicado":
                worksheet.write(row, col, res, format_zero)
            else:
               worksheet.write(row, col, res, format_Premio)
            row += 1
        # Workbook save
        writer.save()
    except OSError:
        print(" - Excel composition error[{}]".format(i))

    print(" - Mailing Composition")
    try:
        # Outlook instance call
        outlook = win32.Dispatch('outlook.application')
        # Mail body create
        mail = outlook.CreateItem(0)
        mail.SentOnBehalfOfName = mail_list['FromBox']
        mail.To = mail_list['to']
        mail.CC = mail_list['cc']
        mail.Subject = 'COE - Vencimento de Operações -' + " dia " + str(painel.loc[0, 'Data de Vencimento'])[:10]
        mail.Body = 'Caros bom dia, \n \n Segue anexo todas as operações de COE que venceram hoje, dia {}, ' \
                    'com suas respectivas rentabilidades. \n \n Qualquer dúvida estamos à disposição.'.format(
            str(painel.loc[0, 'Data de Vencimento'])[:10])

        # Excel Attachment
        attachment = excel_path
        mail.Attachments.Add(attachment)
        # Display mail composition for user
        mail.Send()
        print(" - Mail Send - Check Outbox")
    except OSError:
        print(" - Mail composition Error[{}]".format(i))

    print("Saving Log...")
    # Check log existence
    cursor = con.cursor()
    cursor.execute(
        'select * from enviados where data = "{}"'.format(str(painel.loc[0, 'Data de Vencimento'])[:10]))
    log_check = cursor.fetchall()

    if len(log_check) == 0:
        cursor.execute("""INSERT INTO enviados (nome, data) VALUES (?,?)""",
                       ["Vencimentos COE - " + str(painel.loc[0, 'Data de Vencimento'])[:10] + ".xlsx",
                        str(painel.loc[0, 'Data de Vencimento'])[:10]])
        con.commit()
        con.close()
        print("  - Log done")
    else:
        print(" - File has being already send")
        print(log_check)
        con.close()
    print("\n")
    print("Press Enter to exit...")
    vl = input()